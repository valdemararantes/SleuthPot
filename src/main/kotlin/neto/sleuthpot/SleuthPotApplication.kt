package neto.sleuthpot

import org.springframework.boot.autoconfigure.SpringBootApplication
import org.springframework.boot.runApplication

@SpringBootApplication
class SleuthPotApplication

fun main(args: Array<String>) {
    runApplication<SleuthPotApplication>(*args)
}

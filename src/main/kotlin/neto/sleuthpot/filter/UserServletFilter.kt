package neto.sleuthpot.filter

import org.slf4j.MDC
import org.springframework.security.core.context.SecurityContextHolder
import java.io.IOException
import java.security.Principal
import javax.servlet.*
import javax.servlet.annotation.WebFilter
import javax.servlet.http.HttpServletRequest


/**
 * Created by Valdemar on 12/01/2017.
 */
@WebFilter("/")
class UserServletFilter : Filter {

    private val USER_KEY = "username"

    override fun destroy() {}

    override fun doFilter(request: ServletRequest, response: ServletResponse,
                          chain: FilterChain) {

        var successfulRegistration = false
        val req = request as HttpServletRequest
        // Please note that we could have also used a cookie to
        // retrieve the user name
        val username = req.userPrincipal?.name ?: SecurityContextHolder.getContext().authentication?.name

        if (username != null) {
            successfulRegistration = registerUsername(username)
        }

        try {
            chain.doFilter(request, response)
        } finally {
            if (successfulRegistration) {
                MDC.remove(USER_KEY)
            }
        }
    }

    override fun init(arg0: FilterConfig) {
    }

    /**
     * Register the user in the MDC under USER_KEY.

     * @param username
     * *
     * @return true id the user can be successfully registered
     */
    private fun registerUsername(username: String?): Boolean {
        if (username != null && username.trim { it <= ' ' }.isNotEmpty()) {
            MDC.put(USER_KEY, username)
            return true
        }
        return false
    }
}

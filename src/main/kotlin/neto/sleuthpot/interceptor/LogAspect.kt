package neto.sleuthpot.interceptor

import org.aspectj.lang.ProceedingJoinPoint
import org.aspectj.lang.annotation.Around
import org.aspectj.lang.annotation.Aspect
import org.aspectj.lang.annotation.Pointcut
import org.slf4j.LoggerFactory
import org.springframework.stereotype.Component
import java.time.Duration
import java.time.Instant

@Component
@Aspect
class LogAspect {

    private val log = LoggerFactory.getLogger(javaClass)

    @Pointcut("bean(*Controller) && within(neto.sleuthpot..*)")
    private fun controllers(){}

    @Around("controllers()")
    fun logMethodsInvocation(joinPoint: ProceedingJoinPoint): Any? {
        val typeName = joinPoint.signature.declaringType.simpleName
        val methName = joinPoint.signature.name
        val name = "$typeName.$methName"
        val initialInstant = Instant.now()
        log.info("Invoking $name")
        try {
            return joinPoint.proceed()
        } finally {
            log.info("$name executed in ${Duration.between(initialInstant, Instant.now())}")
        }
    }
}